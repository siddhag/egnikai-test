package GenericLib;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationSetup {

    private static ConfigurationSetup configurationSetup;
    private static String candidateUrl;
    private static String adminUrl;
    private static String DBUrl;
    private static String UserApiURI;

    public static ConfigurationSetup getInstance() {
        if (configurationSetup == null) {
            configurationSetup = new ConfigurationSetup();
            configurationSetup.loadData();
        }
        return configurationSetup;
    }

    private void loadData() {

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("./src/test/resources/Data/Egnikai" + (System.getProperty("env")).toUpperCase() + "Configuration.properties"));
        } catch (IOException e) {
            System.out.println("Data is not loaded from property file");
        }
        candidateUrl = prop.getProperty("candidateUrl");
        adminUrl = prop.getProperty("adminUrl");
        DBUrl = prop.getProperty("DBUrl");
        UserApiURI = prop.getProperty("UserApiURI");

    }


    public String getCandidateUrl() {
        return candidateUrl;
    }

    public String getAdminUrl() {
        return adminUrl;
    }

    public String getDBUrl() {
        return DBUrl;
    }

    public String getUserApiURI() {
        return UserApiURI;
    }


}
