package GenericLib;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public  class ScreenShot
{

    public static  String takeScreenshot(WebDriver driver, String fileName)
    {
        TakesScreenshot t= (TakesScreenshot)driver;
        Date d=new Date();
        String date=d.toString().replace(":", "-");
        String dst="snapshots/"+fileName+date+".png";
        File srcFile=t.getScreenshotAs(OutputType.FILE);
        File dstfile= new File(dst);


        try
        {
            FileUtils.copyFile(srcFile, dstfile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return fileName+date+".png";
    }
}
