package GenericLib;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class CsvFunctions
{
    private static final String SAMPLE_CSV_FILE_PATH = "/Users/amartans/Downloads/Candidates-list.csv";
    public String eventId;

    public void test(String filename) throws IOException
    {
        Reader in = new FileReader(filename);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
        for (CSVRecord record : records) {
            String userName = record.get(0);
            System.out.println(userName);

            String xxxx = record.get(1);
            System.out.println(xxxx);

            String userid = record.get(2);
            System.out.println(userid);

            String password = record.get(2);
            System.out.println(password);


        }
    }


    public static void readCSV() throws IOException
    {
        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withHeader("eventid", "candidateName", "userId", "password","challengeTitle","createdAt","score")
                        .withIgnoreHeaderCase()
                        .withTrim())
        ) {
            for (CSVRecord csvRecord : csvParser) {
                // Accessing values by the names assigned to each column
                String eventid = csvRecord.get("eventid");
                String candidateName = csvRecord.get("candidateName");
                String userId = csvRecord.get("userId");
                String password = csvRecord.get("password");
                String challengeTitle = csvRecord.get("challengeTitle");
                String createdAt = csvRecord.get("createdAt");
                String score = csvRecord.get("score");

                System.out.println("Record No - " + csvRecord.getRecordNumber());
                System.out.println("---------------");
                System.out.println("Name : " + eventid);
                System.out.println("Email : " + candidateName);
                System.out.println("Phone : " + userId);
                System.out.println("Country : " + password);
                System.out.println("Name : " + challengeTitle);
                System.out.println("Email : " + createdAt);
                System.out.println("Phone : " + score);
                System.out.println("---------------\n\n");
            }

            System.out.println(csvParser.getHeaderMap());

        }
    }

    public void generate()
    {
        String csvFilePath = "/Users/amartans/Downloads/Candidates-list.csv";
        Workbook wb = new XSSFWorkbook(); //You can use XSSFWorkbook to generate .xlsx files
        FileOutputStream fileOut;
        try
        {
            ICsvMapReader csvMapReader = new CsvMapReader(new FileReader(
                    csvFilePath), CsvPreference.EXCEL_PREFERENCE);
            final String[] headers = csvMapReader.getHeader(true);


            eventId=headers[0];



            Map csvRow;

            fileOut = new FileOutputStream("/Users/amartans/Downloads/Candidates-list1.xlsx");
            Sheet sheet = wb.createSheet("products");

            int rowNo = 0;
            createExcelHeader(sheet, rowNo);

            while ((csvRow = csvMapReader.read(headers)) != null)
            {
                rowNo++;
                Row excelRow = sheet.createRow(rowNo);
                createExcelRow(csvRow, excelRow);
            }

            csvMapReader.close();
            wb.write(fileOut);
            fileOut.close();
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void createExcelHeader(Sheet sheet, int rowNo) {
        Row excelRow = sheet.createRow(rowNo);
        excelRow.createCell(0).setCellValue("eventId");
        excelRow.createCell(1).setCellValue("candidateName");
        excelRow.createCell(2).setCellValue("userId");
        excelRow.createCell(3).setCellValue("password");
        excelRow.createCell(4).setCellValue("challengeTitle");
        excelRow.createCell(5).setCellValue("createdAt");
        excelRow.createCell(6).setCellValue("score");


    }

    public void createExcelRow(Map csvRow, Row excelRow)
    {
        excelRow.createCell(0).setCellValue((String)csvRow.get(eventId));
        excelRow.createCell(1).setCellValue((String)csvRow.get("candidateName"));
        excelRow.createCell(2).setCellValue((String)csvRow.get("userId"));
        excelRow.createCell(3).setCellValue((String)csvRow.get("password"));
        excelRow.createCell(4).setCellValue((String)csvRow.get("challengeTitle"));
        excelRow.createCell(5).setCellValue((String)csvRow.get("createdAt"));
        excelRow.createCell(6).setCellValue((String)csvRow.get("score"));
    }



}
