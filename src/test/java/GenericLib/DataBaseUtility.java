package GenericLib;

import org.testng.Assert;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DataBaseUtility
{
    private static String  url=  ConfigurationSetup.getInstance().getDBUrl();
    public  static void validateSingleValueDataFromDB(String sqlQuery, String expectedValue)

    {
        String columnsValue=null;
        ResultSet rs;
        try
        {
            Connection conn = DriverManager.getConnection(url,"root","nimda");

            if (conn != null)
            {
                System.out.println("Connected");
            }
            Statement st = conn.createStatement();
            rs = st.executeQuery(sqlQuery);

            while(rs.next())
            {
                for(int i=1;i<=1;i++)
                {
                    columnsValue=rs.getString(i);

                }

            }

            Assert.assertEquals(columnsValue,expectedValue);

            conn.close();

        }

        catch(Exception e)
        {
            System.out.print(e);
            System.out.println("failed to get the data");
        }
    }

    public  static String fetchingSingleValueDataFromDB(String sqlQuery)

    {
        String columnsValue=null;
        ResultSet rs = null;
        try
        {

            Connection conn = DriverManager.getConnection(url,"root","nimda");

            if (conn != null)
            {
                System.out.println("Connected");
            }
            Statement st = conn.createStatement();
            rs = st.executeQuery(sqlQuery);

            while(rs.next())
            {
                for(int i=1;i<=1;i++)
                {
                    columnsValue=rs.getString(i);

                }

            }
            conn.close();

        }

        catch(Exception e)
        {
            System.out.print(e);
            System.out.println("failed to get the data");
        }

        return columnsValue;
    }

    public  static HashMap<String,String>  fetchingMultipleValueDataFromDB(String sqlQuery)

    {
        HashMap<String,String> userCredential = new HashMap<String, String>();
        ResultSet rs = null;
        try
        {

            Connection conn = DriverManager.getConnection(url,"root","nimda");

            if (conn != null)
            {
                System.out.println("Connected");
            }
            Statement st = conn.createStatement();
            rs = st.executeQuery(sqlQuery);
            ResultSetMetaData meta = rs.getMetaData();
            int columnSize=meta.getColumnCount();

            while(rs.next())
            {
                for(int i=1;i<=columnSize;i++)
                {
                    userCredential.put(meta.getColumnName(i),rs.getString(i));

                }

            }
            conn.close();

        }

        catch(Exception e)
        {
            System.out.print(e);
            System.out.println("failed to get the data");
        }

        return userCredential;
    }
}
