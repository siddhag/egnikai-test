package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by vigneshi on 6/11/18.
 */
public class Login {

    SelenideElement username = $(byId("username"));
    SelenideElement password = $(byId("password"));
    SelenideElement loginButton = $(byClassName("login-button"));
    SelenideElement landingPageLabel = $(byClassName("title"));
    SelenideElement errorMessage = $(byClassName("has-text-danger"));
    SelenideElement landingTitle = $(By.partialLinkText("Hello"));


    private String assertText;

    public void loginToEgnikai(String userName, String passWord) {
        username.sendKeys(userName);
        password.sendKeys(passWord);
        loginButton.click();
    }

    /*public void inBuiltWait(){
        landingPageLabel.waitUntil(Condition.visible,3000);
    }


    public void positiveValidation(String displayName){
        landingPageLabel.shouldHave(Condition.matchesText(displayName));
    }

    public void negativeValidation(){
        errorMessage.shouldHave(Condition.visible);
        loginButton.shouldHave(Condition.visible);
    }*/
}
