package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;




public class EgnikaiHome {

    protected final SelenideElement readInstructionsButton =$(byXpath("//button[@class='button is-primary '].span[2]"));

    protected final SelenideElement instructionLabel = $(byText("Instructions"));
    protected final SelenideElement seeSampleProblem = $(byText("See sample problem"));
    protected final SelenideElement sampleProblemLabel = $(byText("Sample Problem"));
    protected final SelenideElement startTestButton = $(byText("Start test"));
    protected final SelenideElement challengePage = $(byText("Problems"));
    protected final SelenideElement nextButton = $(byText("Next"));



//    public void wipGoTo(){
//        readInstructions.waitUntil(Condition.visible, 3000);
//        readInstructions.click();
//        instructionLabel.shouldHave(Condition.visible);
//        seeSampleProblem.click();
//        sampleProblemLabel.shouldHave(Condition.visible);
//        startTestButton.click();
//    }

    public void goToNextSteps(SelenideElement element)
    {
        // to go the next page

      element.waitUntil(Condition.visible,3000);
      element.click();


    }

    public void validateNextSteps(SelenideElement element){
        element.waitUntil(Condition.visible, 5000);
        element.shouldHave(Condition.visible);
//        element.shouldHave(Condition.matchesText(verifyText));
    }

    public boolean refreshChallengePage()  {
        WebDriverRunner.getWebDriver().navigate().refresh();
        //Thread.sleep(5000);
        boolean b =challengePage.isDisplayed();
        return b;
    }


}
