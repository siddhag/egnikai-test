package tests;

import GenericLib.ConfigurationSetup;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.SelenideElement.*;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;


/**
 * Created by vigneshi on 6/11/18.
 */
public class BaseTest {
    private final SelenideElement logout = $(withText("Finish"));

    @Parameters({"browser"})
    @BeforeMethod(alwaysRun = true)
    public void setup(String browser, Method method) {
        System.out.println(ConfigurationSetup.getInstance().getCandidateUrl());
        if (!method.getName().matches("deactivateUser|createUser")) {
            setUpBrowser(browser);
            switchTo().window(0); }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(){
        Selenide.close();
    }

    private void setUpBrowser(String browser) {
        if(browser.equalsIgnoreCase("chrome")) {
            System.out.printf("Browser" + browser);
            Configuration.browser = WebDriverRunner.CHROME;
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, "./src/test/resources/webdriver/chromedriver");
        }
        else if(browser.equalsIgnoreCase(" Mozillla firefox")) {
            Configuration.browser = WebDriverRunner.FIREFOX;
            System.setProperty(FirefoxDriver.BINARY, "./src/test/resources/webdriver/geckodriver");
        }
    }
}
