package tests;

import BusinessLib.ChallengesLib;
import BusinessLib.UserApiUtility;
import BusinessLib.LoginFunction;
import EgnikaiObjects.ChallengeObj;
import EgnikaiObjects.LoginScreenObj;
import EgnikaiObjects.ProblemsObj;
import GenericLib.ConfigurationSetup;
import GenericLib.DataBaseUtility;
import GenericLib.FileHandling;
import com.codeborne.selenide.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.EgnikaiHome;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;

public class CandidateTest extends BaseTest2 {

    LoginFunction loginFunction = new LoginFunction();
    ChallengesLib challengesLib = new ChallengesLib();
    LoginScreenObj loginScreenObj = new LoginScreenObj();
    ChallengeObj challengeObj = new ChallengeObj();
    ProblemsObj problem = new ProblemsObj();
    UserApiUtility uapiUtility = new UserApiUtility();
    static String  user_id;
    static String password;
    String filepath=null;

    Logger logger = Logger.getLogger(CandidateTest.class.getName());


    @Test()
    public void createUser()
    {
       HashMap<String,String> credentails = UserApiUtility.createUser();
        user_id=credentails.get("user_id");
        password =credentails.get("password");
        assertNotNull(user_id,"User is not created");
        System.out.println(user_id+"  "+password);
    }

    @Test(dependsOnMethods = {"createUser"})
    public void loginWithValidUsername() {
        Selenide.open(ConfigurationSetup.getInstance().getCandidateUrl());
        loginScreenObj.getUserNameEdt().sendKeys(user_id);
        loginScreenObj.getPasswordEdt().sendKeys(password);
        assertTrue(loginScreenObj.getLoginBtn().isDisplayed(), "Login Button is not displayed");
        loginScreenObj.getLoginBtn().click();
        loginScreenObj.getLoginBtn().shouldBe(Condition.disappear);
        assertTrue(loginScreenObj.welcomeToEgnikaiIsDisplayed(), "Welcome to Egnikai message is not displayed");
        assertTrue(loginScreenObj.getReadInstruction().isDisplayed(), "Read instruction button is not displayed");
        loginScreenObj.getReadInstruction().click();
        assertTrue(loginScreenObj.InstructionPageisDisplayed(), "Instruction page is not displayed");
        loginScreenObj.getNextBtn().click();
        loginScreenObj.getStartYourButton().waitUntil(Condition.enabled, 20000).click();
        assertTrue(challengeObj.challengePageIsDisplayed(), "Problem page is not displayed");
    }
    @Test(dependsOnMethods = {"createUser"})
    public void loginWithInvalidCredentials() {
        Selenide.open(ConfigurationSetup.getInstance().getCandidateUrl());
        loginScreenObj.getUserNameEdt().sendKeys(user_id);
        loginScreenObj.getPasswordEdt().sendKeys("fff");
        assertTrue(loginScreenObj.getLoginBtn().isDisplayed(), "Login Button is not displayed");
        loginScreenObj.getLoginBtn().click();
        assertTrue(loginScreenObj.getInvalidLoginMessage().equalsIgnoreCase("*Invalid username or password"));

    }

    @Test(dependsOnMethods = {"createUser","loginWithValidUsername"})
    public void attemptProblemWithSucess() throws IOException, InterruptedException {
        int problemNo = 2;
        System.out.println("user_id::: " +  user_id);
        System.out.println("password " +  password);

        loginFunction.loginToApp(user_id, password);
        challengeObj.selectTheProblem(problemNo);
        assertTrue(challengesLib.correctProblemIsDisplayed(problemNo), "correct problem is not displayed");

        String input = FileHandling.readFile("./src/test/resources/Data/problem" + problemNo + ".txt", Charset.defaultCharset());

        problem.getProblemTextbox().hover().click();
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(Keys.chord(Keys.COMMAND, "a")).build().perform();
        Thread.sleep(2000);
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(input).build().perform();
        problem.getSaveBtn().waitUntil(Condition.enabled, 10000).click();
        problem.getRunBtn().waitUntil(Condition.enabled, 10000).click();
        problem.getResult().waitUntil(Condition.visible, 10000);
        assertTrue(challengesLib.testResult().equalsIgnoreCase("Test case passed"));

        Thread.sleep(30000);


        HashMap<String, String> Score = DataBaseUtility.fetchingMultipleValueDataFromDB("SELECT COALESCE(sum(d.credit),0) as TotalCredit,COALESCE(a.score,0) as TotalScore FROM candidate a\n" +
                "INNER JOIN candidate_attempted_problems b\n" +
                "ON a.candidate_id=b.candidate_candidate_id\n" +
                "INNER JOIN attempted_problem c\n" +
                "ON b.attempted_problems_attempted_problem_id=c.attempted_problem_id\n" +
                "INNER JOIN problem d\n" +
                "ON c.problem_problem_id=d.problem_id\n" +
                "WHERE a.user_id=\"" + user_id + "\" AND c.is_passed=1;");

        System.out.println("Credit value is::::::::: "+ Score.get("TotalCredit"));
        System.out.println("Total score value is::::::::: "+ Score.get("TotalScore"));


        assertEquals(Score.get("TotalCredit"), Score.get("TotalScore"), "Scrore is not matching");

    }
  @Test(dependsOnMethods ={"loginWithValidUsername","attemptProblemWithSucess"},alwaysRun = true)
    public void deactivateUser()
    {

        UserApiUtility.Deactivate(DataBaseUtility.fetchingSingleValueDataFromDB("SELECT candidate_id FROM candidate WHERE user_id=\""+user_id+"\" "));
    }

    @Test(description = "Finish functionality: Once the user finish the challenges then he/she should not able to login to applicaiton.")
    public void welcomeMessage() throws InterruptedException {
        loginFunction.loginToApp("testuser", "testuser");
        loginFunction.finish();
        logger.info("Candidate finished the challenges");
        loginScreenObj.getlifeatTW().waitUntil(Condition.visible,8000);
        //Relogin again
        WebDriverRunner.getWebDriver().navigate().back();
        loginScreenObj.getLoginBtn().waitUntil(Condition.visible,4000);
        //loginFunction.loginToApp("testuser", "testuser");
        loginScreenObj.getUserNameEdt().sendKeys("Testuser");
        loginScreenObj.getPasswordEdt().sendKeys("Testuser");
        loginScreenObj.getLoginBtn().waitUntil(Condition.visible,2000).click();
        String invalidMsg= loginScreenObj.getinvalidLoginMsg().waitUntil(Condition.visible,3000).getText();
        System.out.println("invalidMsg:: " + invalidMsg);
        Assert.assertEquals("*Invalid username or password",invalidMsg,"Both the message");
        logger.info("Finish functionality is working properly");
        //Unlock the user
        UserApiUtility.Unlock(DataBaseUtility.fetchingSingleValueDataFromDB("SELECT candidate_id FROM candidate WHERE user_id=\""+"testuser"+"\" "));
    }


    @Test
    public void notlogoutbrowserclose()
    {
        HashMap<String, String> userData = UserApiUtility.createUser();
        loginFunction.loginToApp(userData.get("user_id"),userData.get("password"));
        Selenide.close();
        //switchTo().window(0);
        Configuration.browser= WebDriverRunner.CHROME;
        loginFunction.loginToApp(userData.get("user_id"),userData.get("password"));
    }

   // @Test
    public void CSharpTest() throws InterruptedException, IOException {
        int problemNo = 6;
        loginFunction.loginToApp("Ctest7888", "3fc374");
        challengeObj.selectTheProblem(problemNo);
        String input = FileHandling.readFile("./src/test/resources/Data/CSharpProblem" + problemNo + "_SalaryIncrementManager.txt", Charset.defaultCharset());

        problem.getProblemTextbox().hover().click();
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(Keys.chord(Keys.COMMAND, "a")).build().perform();
        Thread.sleep(3000);
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(input).build().perform();
        problem.getSaveBtn().waitUntil(Condition.enabled, 10000).click();

        problem.getRunBtn().waitUntil(Condition.visible,4000);
        problem.getSrcCodeFile().click();
        String input2 = FileHandling.readFile("./src/test/resources/Data/CSharpProblem" + problemNo + "_Employee.txt", Charset.defaultCharset());
        problem.getProblemTextbox().hover().click();
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(Keys.chord(Keys.COMMAND, "a")).build().perform();
        Thread.sleep(3000);
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(input2).build().perform();
        problem.getSaveBtn().waitUntil(Condition.enabled, 10000).click();



        problem.getRunBtn().waitUntil(Condition.enabled, 10000).click();
        problem.getResult().waitUntil(Condition.visible, 130000);
        assertTrue(challengesLib.testResult().equalsIgnoreCase("Test case passed"));

        HashMap<String, String> Score = DataBaseUtility.fetchingMultipleValueDataFromDB("SELECT COALESCE(sum(d.credit),0) as TotalCredit,COALESCE(a.score,0) as TotalScore FROM candidate a\n" +
                "INNER JOIN candidate_attempted_problems b\n" +
                "ON a.candidate_id=b.candidate_candidate_id\n" +
                "INNER JOIN attempted_problem c\n" +
                "ON b.attempted_problems_attempted_problem_id=c.attempted_problem_id\n" +
                "INNER JOIN problem d\n" +
                "ON c.problem_problem_id=d.problem_id\n" +
                "WHERE a.user_id=\"" + user_id + "\" AND c.is_passed=1;");


        assertEquals(Score.get("TotalCredit"), Score.get("TotalScore"), "Scrore is not matching");
    }


    //Existing issue
    @Test
    public void hitChallengePageUrlTwice() throws InterruptedException {
        loginFunction.loginToApp("sethu","sethu");
        EgnikaiHome egnikaiHome =new EgnikaiHome();
        assertTrue(egnikaiHome.refreshChallengePage());
        System.out.printf("Passed through assertion");

    }

}
