package tests;

import BusinessLib.Admin;
import BusinessLib.UserApiUtility;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class AdminLoginTest extends BaseTest2
{
    Admin admin = new Admin();
    String userName;

    @Test()
    public void createUser()
    {
         userName = UserApiUtility.createUser().get("user_name");
         System.out.println(userName+"****************");
    }

    @Test(description = "Testing Admin Login Functionality",dependsOnMethods = {"createUser"})
    public void Test_Admin_Login() throws InterruptedException {
        admin.loginToAdmInApp("admin","admin");

    }

     @Test(description = "Testing Admin User Lock Functionality",dependsOnMethods = {"createUser"})
    public void Test_Admin_UserLock() throws InterruptedException {
        admin.loginToAdmInApp("admin","admin");
        admin.lockUser(userName);
        admin.verifyUserIsLocked(userName);
    }

    @Test(description = "Testing Admin User Unlock Functionality",dependsOnMethods = {"Test_Admin_UserLock","createUser"})
    public void Test_Admin_UserUnLock() throws InterruptedException {
         admin.loginToAdmInApp("admin","admin");
        admin.unLockUser(userName);
        admin.verifyUserIsUnLocked(userName);
    }

    @Test(description = "Testing Admin User Deactivation Functionality",dependsOnMethods = {"Test_Admin_UserUnLock","createUser"})
    public void Test_Admin_UserDeactivation() throws InterruptedException {
        admin.loginToAdmInApp("admin","admin");
        admin.deactivateUser(userName);
        admin.verifyUserIdDeactivated(userName);
    }


    @Test
    public void checking_null_user() throws IOException, InterruptedException {
        admin.loginToAdmInApp("admin","admin");
        admin.createCandidateViaweb("admin","admin","adding null","empty");
        List<String> ls = admin.candidateNames_checkingnulluser();
        for(int i=0;i<ls.size();i++)
        {
            ls.get(i).trim().replaceAll(" +", "");
            if (ls.get(i).equals(""))
                Assert.assertFalse(true);
        }

    }


}
