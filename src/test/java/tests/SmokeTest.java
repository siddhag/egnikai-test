package tests;

import BusinessLib.Admin;
import BusinessLib.ChallengesLib;
import BusinessLib.LoginFunction;
import EgnikaiObjects.ProblemsObj;
import GenericLib.FileHandling;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import pages.EgnikaiHome;
import pages.Login;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by vigneshi on 6/12/18.
 */
public class SmokeTest extends BaseTest {

    Login login = page(Login.class);
    ChallengesLib challenge = new ChallengesLib();
    ProblemsObj problem = new ProblemsObj();
    EgnikaiHome home = page(EgnikaiHome.class);

    //data objects
    protected static SelenideElement readInstructions = $(byText("Read instructions"));
    protected static SelenideElement instructionLabel = $(byText("Instructions"));
    protected static SelenideElement seeSampleProblem = $(byText("See sample problem"));
    protected static SelenideElement sampleProblemLabel = $(byText("Sample Problem"));
    protected static SelenideElement startTestButton = $(byText("Start your"));

    @Test
    public void loginWithValidUsername() throws InterruptedException, IOException {
        LoginFunction loginFunction = new LoginFunction();
        Admin admin = new Admin();

        loginFunction.loginToApp("testuser", "testuser");
        challenge.selectTheProblem(2);

        String input = FileHandling.readFile("./sample.txt", Charset.defaultCharset());
        System.out.println(input);

        problem.getProblemTextbox().hover().click();
        actions().moveToElement(problem.getProblemTextbox()).sendKeys(Keys.chord(Keys.COMMAND, "a")).build().perform();

        actions().moveToElement(problem.getProblemTextbox()).sendKeys(input).build().perform();
        problem.getSaveBtn().waitUntil(Condition.visible, 10000).click();

        problem.getRunBtn().waitUntil(Condition.visible, 10000).click();
        problem.getSeeAllProblems().waitUntil(Condition.visible, 1000).click();

    }
}