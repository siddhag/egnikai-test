package tests;

import GenericLib.ConfigurationSetup;
import GenericLib.ScreenShot;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.switchTo;


/**
 * Created by vigneshi on 6/11/18.
 */
public class BaseTest2 {

    private static ExtentHtmlReporter htmlReporter;
    private static ExtentReports extent;
    private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();

    @BeforeSuite
    public void settingUpReport() {
        Date d=new Date();
        String date=d.toString().replace(":", "-");
        htmlReporter= new ExtentHtmlReporter("test-reports/SmokeTestReport_"+date+".html");
        extent= new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("Opereating System", "Mac OS");
        extent.setSystemInfo("Environment", "QA");
        extent.setSystemInfo("Host Name", "Amar");
        extent.setSystemInfo("User", "Amar");

        htmlReporter.config().setDocumentTitle("Egnikai AutomationTestReport");
        htmlReporter.config().setReportName("Egnikai AutomationTestReport");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setChartVisibilityOnOpen(false);
    }

    @BeforeClass
    public synchronized void beforeClass() {
        ExtentTest parent = extent.createTest(getClass().getName());
        parentTest.set(parent);
    }

    @Parameters({"browser"})
    @BeforeMethod(alwaysRun = true)
    public void setup(String browser, Method method) {
        System.out.println(ConfigurationSetup.getInstance().getCandidateUrl());
        if (!method.getName().matches("deactivateUser|createUser")) {
            setUpBrowser(browser);
            switchTo().window(0); }

        ExtentTest child = parentTest.get().createNode(method.getName());
        test.set(child);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result)
    {
        WebDriver driver=WebDriverRunner.getWebDriver();
        System.out.println("after  "+driver);
        if(result.getStatus()==ITestResult.FAILURE)
        {
            String fileName = ScreenShot.takeScreenshot(driver, result.getName());
            test.get().log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" failed becuase of below issue", ExtentColor.RED));
            test.get().fail(result.getThrowable());
            try {
                File file = new File("snapshots/"+fileName);
                String absolutePath = file.getAbsolutePath();
                System.out.println(absolutePath);
                test.get().addScreenCaptureFromPath(absolutePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            close();
        }
        else if(result.getStatus()==ITestResult.SUCCESS)
        {
            test.get().log(Status.PASS, MarkupHelper.createLabel(result.getName()+" is pass ", ExtentColor.GREEN));
           close();
        }

        else {
            test.get().log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " is skipped because of below issue ", ExtentColor.ORANGE));
            test.get().skip(result.getThrowable());
            close();
        }
    }

    @AfterSuite
    public void flushingReport()
    {
        extent.flush();
    }

    private void setUpBrowser(String browser) {
        if(browser.equalsIgnoreCase("chrome")) {
            Configuration.browser = WebDriverRunner.CHROME;
        }
        else if(browser.equalsIgnoreCase(" Mozillla firefox")) {
            Configuration.browser = WebDriverRunner.FIREFOX;
        }
    }
}
