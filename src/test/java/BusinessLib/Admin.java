package BusinessLib;

import EgnikaiObjects.AdminLoginObj;
import EgnikaiObjects.LoginScreenObj;
import GenericLib.ConfigurationSetup;
import GenericLib.DataBaseUtility;
import GenericLib.SelenideUtility;
import com.codeborne.selenide.*;
import com.codeborne.selenide.commands.ShouldHave;
import com.codeborne.selenide.commands.ShouldNotHave;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.testng.Assert.assertEquals;

public class Admin {
    LoginScreenObj login = new LoginScreenObj();
    AdminLoginObj adminScreen = new AdminLoginObj();

    public void loginToAdmInApp(String userName, String password) {
        Selenide.open(ConfigurationSetup.getInstance().getAdminUrl());
        login.getUserNameEdt().sendKeys(userName);
        login.getPasswordEdt().sendKeys(password);
        System.out.println("Login button test::::" + adminScreen.getLoginAdminBtn().isDisplayed());
        adminScreen.getLoginAdminBtn().shouldHave(Condition.visible).click();
    }

    public void createCandidateViaweb(String userName, String password, String eventName,String filetype) throws InterruptedException, IOException {
        loginToAdmInApp(userName, password);
        adminScreen.getCreateEventBtn().shouldHave(Condition.visible).click();
        adminScreen.getEventNameEdit().sendKeys(eventName);
        SelenideElement fileUploadElement = $(byXpath("//input[@type='file']"));
        Thread.sleep(2000);
        String path=null;
        if(filetype.equals("empty"))
            path = new File(".").getCanonicalPath() + "/src/test/resources/Data/Create_CandidateList_null.csv";
        else
            path = new File(".").getCanonicalPath() + "/src/test/resources/Data/Create_CandidateList.csv";
        fileUploadElement.sendKeys(path);
        SelenideElement selectChallenge = $("select");
        selectChallenge.selectOptionByValue("3");
        System.out.println("Selected text from the options" + selectChallenge.getText());
        selectChallenge.selectOption("Java 8 Sample");
        assertEquals(selectChallenge.getSelectedText(), "Java 8 Sample");
        System.out.println("Selected text from the options" + selectChallenge.getText());
        adminScreen.getApplyAllbtn().shouldBe(Condition.visible).click();
        adminScreen.getSubmitBtn().should(Condition.visible).click();
    }

    public void navigateToActiveSession() {
        adminScreen.getActiveSessionLink().shouldBe(Condition.visible).click();
    }

    public void navigateToPastSession() {
        adminScreen.getPastSessionLink().shouldBe(Condition.visible).click();
    }

    public void navigateToCreateEvent() {
        adminScreen.getCreateEventBtn().shouldBe(Condition.visible).click();
    }

    public void candidateNames() {
        List<SelenideElement> candidateNames = $$(byXpath("//table[@class='table table-striped table-sm']/tbody/tr/td[3]"));
        System.out.println("Total candidates are::: " + candidateNames.size());
        List<SelenideElement> totalRow = $$(byXpath("//table[@class='table table-striped table-sm']/tbody/tr"));
        System.out.println("Total Row is::: " + totalRow.size());
    }

    public int findCandidateRow(String candidateName) {
        Table candidateTable = new Table($(byXpath("//table[@class='table table-striped table-sm']")));
        List<SelenideElement> candidateNamesRow = candidateTable.getColumn(3);
        for (int i = 0, size = candidateNamesRow.size(); i <= size; i++) {
            if (candidateName.equals(candidateNamesRow.get(i).getText())) {
                return i + 1;
            }
        }
        return -1;
    }

    public List<String> candidateNames_checkingnulluser() {


        List<String> userName = new ArrayList<>();
        ElementsCollection rowcount = $$x("//table//tbody//tr//td[2]");
        System.out.println("rowcount size " + rowcount.size());
        for (int i = 0; i < rowcount.size(); i++) {
            System.out.println("value of i= " + i);
            System.out.println("candidate name is " + rowcount.get(i).getText());
            String s = rowcount.get(i).getText();
            userName.add(s);
        }

        return userName;

    }

    public void lockCandidate(String candidateName) {
        Table candidateTable = new Table($(byXpath("//table[@class='table table-striped table-sm']")));
        int candidateRow = findCandidateRow(candidateName);
        candidateTable.getItemAt(candidateRow, 6).find("i").click();
    }

    public void lockUser(String userName) {
        adminScreen.getLockCandidateIcon(userName).shouldHave(Condition.visible).click();
    }

    public void unLockUser(String userName) {
        adminScreen.getLockCandidateIcon(userName).shouldHave(Condition.visible).click();
    }

    public void verifyUserIsLocked(String userName) {
        String query = "select is_locked from candidate where user_id= (select user_id from `user` where user_name='" + userName + "')";
        DataBaseUtility.validateSingleValueDataFromDB(query, "1");
    }

    public void verifyUserIsUnLocked(String userName) {
        String query = "select is_locked from candidate where user_id= (select user_id from `user` where user_name='" + userName + "')";
        DataBaseUtility.validateSingleValueDataFromDB(query, "0");
    }

    public void deactivateUser(String userName) {
        adminScreen.getDeactivateButton(userName).shouldHave(Condition.visible).click();
    }

    public void verifyUserIdDeactivated(String userName) {
        adminScreen.getPastSessionLink().click();
        adminScreen.getCandidateName(userName).shouldHave(Condition.visible);
    }

    public void verifyGitRepoIsCreating(String userName) {
        String cid = adminScreen.getCid(userName).getText();
        adminScreen.getGitRepo(userName).click();
        Selenide.switchTo().window("siddhag / egnikai-java-challenge-sample / source / — Bitbucket");
        Assert.assertEquals(getWebDriver().getCurrentUrl(), "https://bitbucket.org/siddhag/egnikai-java-challenge-sample.git/src/c" + cid);
    }

    @Test
    public void testName() {
    }

    public  void fetchingDataFromUIIntoExcel (String xlName, String sheetName ) throws IOException, SQLException
    {
           @SuppressWarnings("resource")
           XSSFWorkbook workbook1 = new XSSFWorkbook();
           XSSFSheet sheet = workbook1.createSheet(sheetName);
           XSSFRow rowhead = sheet.createRow((short) 0);

            //Setting Header style
            CellStyle style1 = workbook1.createCellStyle();
            style1.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
            style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style1.setBorderBottom(BorderStyle.THIN);
            style1.setBorderLeft(BorderStyle.THIN);
            style1.setBorderRight(BorderStyle.THIN);
            style1.setBorderTop(BorderStyle.THIN);
            XSSFFont font = workbook1.createFont();
            font.setColor(IndexedColors.WHITE.getIndex());
            font.setBold(true);
            style1.setFont(font);

                    //Setting Cell Style
            CellStyle style2 = workbook1.createCellStyle();
            style2.setBorderBottom(BorderStyle.THIN);
            style2.setBorderLeft(BorderStyle.THIN);
            style2.setBorderRight(BorderStyle.THIN);
            style2.setBorderTop(BorderStyle.THIN);

          String[] headers = {"eventId", "candidateName", "userId", "password", "challengeTitle", "createdAt", "score"};

        //Creating excel header
               for (int i = 0; i < headers.length; i++)
               {
                      XSSFCell cell = rowhead.createCell((short) i);
                      cell.setCellValue(headers[i]);
                      cell.setCellStyle(style1);
                      int columnIndex = cell.getColumnIndex();
                      sheet.autoSizeColumn(columnIndex);
          
                }
      
              
               //Fetching data from DB to excel
                ElementsCollection rowcount = $$x("//table//tbody//tr");
      
               for(int j=1;j<=rowcount.size();j++)
               {
                   XSSFRow row = sheet.createRow((short) j);
                  String userName=$x("//table//tbody//tr["+j+"]/td[3]").waitUntil(Condition.visible,6000).getText();
                  DataBaseUtility db1= new DataBaseUtility();
                  HashMap<String, String> data = db1.fetchingMultipleValueDataFromDB("select b.event_id,a.user_id,a.password,c.challenge_title,b.created_at from user a inner join candidate b on a.user_id=b.user_id inner join challenge c on c.challenge_id=b.challenge_id where a.user_name='amar'");
      
                   for(int i=0;i<headers.length;i++)
                   {
                       XSSFCell cell = row.createCell((short) i);
                      if(i==0)
                      {
                          cell.setCellValue(data.get("event_id"));
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
                      else if(i==1)
                      {
                          cell.setCellValue(userName);
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
                      else if(i==2)
                      {
                          cell.setCellValue(data.get("user_id"));
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
                      else if(i==3)
                      {
                          cell.setCellValue(data.get("password"));
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
                      else if(i==4)
                      {
                          cell.setCellValue(data.get("challenge_title"));
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
      
                      else if(i==5)
                      {
                          cell.setCellValue(data.get("created_at"));
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }
                      else
                      {
                          cell.setCellValue($x("//table//tbody//tr["+j+"]/td[4]").getText());
                          cell.setCellStyle(style2);
                          int columnIndex = cell.getColumnIndex();
                          sheet.autoSizeColumn(columnIndex);
                      }

                   }
                  System.out.println("Row"+j+" created");
      
              
               }
      
              
                      
               //Saving the excel
               String path = "/Users/amartans/Downloads/" + xlName + ".xlsx";
              FileOutputStream fileOut;
      
              try
              {
                  fileOut = new FileOutputStream(path);
                  workbook1.write(fileOut);
                  fileOut.close();
                  System.out.println("File created successfully");
              }
              catch (Exception e)
              {
                 System.out.println("Failed to create the file");
              }
          }
}
