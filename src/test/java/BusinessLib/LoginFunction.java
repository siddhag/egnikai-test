package BusinessLib;

import EgnikaiObjects.*;
import GenericLib.ConfigurationSetup;
import com.codeborne.selenide.*;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.testng.Assert;
import tests.CandidateTest;

import java.util.logging.Logger;

import static com.codeborne.selenide.Selenide.*;

public class LoginFunction {
    Logger logger = Logger.getLogger(LoginFunction.class.getName());
    LoginScreenObj login = new LoginScreenObj();
    ChallengeObj ch = new ChallengeObj();

    public void loginToApp(String userName, String password){
        Selenide.open(ConfigurationSetup.getInstance().getCandidateUrl());
        login.getUserNameEdt().sendKeys(userName);
        login.getPasswordEdt().sendKeys(password);
        login.getLoginBtn().click();
        String stringwelcomeText = ch.getwelcomeText().getText();
        logger.info("stringwelcomeText:: "+ stringwelcomeText);
        Assert.assertEquals("Welcome to Egnikai.",stringwelcomeText,"Both the values are matched");
        logger.info("Login to application is successfull");
        login.getLoginBtn().shouldBe(Condition.disappear);
        if (login.getReadInstruction().isDisplayed()) {
            login.getReadInstruction().click();
        }
        if(login.getNextBtn().isDisplayed()){
           login.getNextBtn().click();
        }
         login.getStartYourButton().waitUntil(Condition.enabled, 20000).click();

    }

    public void finish() throws InterruptedException {
        login.getFinishBtn().shouldHave(Condition.visible).click();
        login.getYesBtn().waitUntil(Condition.visible, 10000).click();
    }


}

