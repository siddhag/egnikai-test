package BusinessLib;
import GenericLib.ConfigurationSetup;
import GenericLib.DataBaseUtility;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class UserApiUtility
{

    public static HashMap<String,String>  createUser()

    {
        String userName= DataBaseUtility.fetchingSingleValueDataFromDB("SELECT CONCAT('AutoUser',COUNT(*)+1) FROM user");

        String body ="{\""+"eventId\""+":\"Test1\""+",\"candidateChallengeDTOS\""+":[{\""+"candidateName\""+":\""+userName+"\""+",\""+"challengeId\""+":1}]}";

       RestAssured.baseURI=ConfigurationSetup.getInstance().getUserApiURI();

        given().contentType(ContentType.JSON).accept(ContentType.JSON).auth().preemptive().basic("admin","admin").body(body).when().post("/admin/api/candidates/create/").then().statusCode(200).log().all();


        return DataBaseUtility.fetchingMultipleValueDataFromDB("select user_name,user_id,password   from user where user_name='" + userName + "'");

    }

    public static void Deactivate(String candiate_id)
    {
        RestAssured.baseURI=ConfigurationSetup.getInstance().getUserApiURI();
        String body = "{\""+"isActive\""+":false,\""+"candidateIds\""+":["+candiate_id+"]}";
        given().contentType(ContentType.JSON).accept(ContentType.JSON).auth().preemptive().basic("admin","admin").body(body).when().put("/admin/api/candidates").then().statusCode(200).log().all();
    }

    public static void Unlock(String candiate_id)
    {
        RestAssured.baseURI=ConfigurationSetup.getInstance().getUserApiURI();
        String body = "{\""+"isLocked\""+":false,\""+"candidateIds\""+":["+candiate_id+"]}";
        given().contentType(ContentType.JSON).accept(ContentType.JSON).auth().preemptive().basic("admin","admin").body(body).when().put("admin/api/candidates").then().statusCode(200).log().all();
       }

}
