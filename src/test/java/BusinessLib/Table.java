package BusinessLib;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$$;

public class Table {

    final private SelenideElement table;

    public Table(SelenideElement table) {
        this.table = table;
    }


    public SelenideElement getRow(int row){
        return table.find(byXpath("//tbody/tr["+row+"]"));
    }

    public ElementsCollection getColumn(int column){
        return table.$$(byXpath("//tbody/tr/td["+column+"]"));
    }

    public SelenideElement getItemAt(int row, int column){
        return table.find(byXpath("//tbody/tr["+row+"]/td["+column+"]"));
    }
}
