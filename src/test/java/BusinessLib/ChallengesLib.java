package BusinessLib;

import EgnikaiObjects.ChallengeObj;
import EgnikaiObjects.ProblemsObj;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.actions;
import static com.codeborne.selenide.WebDriverRunner.url;

public class ChallengesLib {


    LoginFunction loginFunction = new LoginFunction();
    ChallengeObj challengeObj = new ChallengeObj();
    ProblemsObj problem = new ProblemsObj();

    public void selectTheProblem(int number) {
        challengeObj.getSolveButton().get(number - 1).shouldBe(Condition.visible).click();
    }

    public boolean correctProblemIsDisplayed(int problemNumber) {


        System.out.println(Integer.parseInt(url().substring(url().length() - 1)));

        if (Integer.parseInt(url().substring(url().length() - 1)) == problemNumber) {
            return true;
        } else
            return false;
    }

    public ChallengeObj clickOnSeeAllProblems() {
        problem.getSeeAllProblems().click();
        return new ChallengeObj();
    }

    public boolean isSaveButtonDisabled() {
        return !problem.getSaveBtn().isEnabled();
    }


    public boolean isRunButtonDisabled() {
        return !problem.getRunBtn().isEnabled();
    }

    public void clickOnSaveButton() {
        if (!isSaveButtonDisabled())
            problem.getSaveBtn().click();
    }

    public void clickOnRunButton() {
        if (!isRunButtonDisabled())
            problem.getRunBtn().click();

    }

    public void sendText(String code) {
        actions().moveToElement(problem.getEditor()).click().sendKeys(Keys.COMMAND, "a").sendKeys(code).build().perform();
    }

    public String testResult()
    {
        problem.getResult().shouldBe(Condition.visible);
       return problem.getTestResult().getText();
    }

}
