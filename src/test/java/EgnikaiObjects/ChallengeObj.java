package EgnikaiObjects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ChallengeObj {

    private static SelenideElement challengePage = $(byText("Problems"));
    private static SelenideElement intructionIcon = $(byTitle("instructions"));
    private static SelenideElement challengeIcon = $(byTitle("challenge"));
    private static List<SelenideElement> solveButton = $$("a[class='link-anchor'] > button");
    private static SelenideElement mandatoryAttribute = $(".tag.is-rounded.is-primary");
    private static SelenideElement finishButton = $("div[class^=column] > button");
    private  static SelenideElement welcomeText = $(byXpath("//h1[@class='title is-size-2 has-text-weight-light']"));

    public boolean challengePageIsDisplayed() {

        return challengePage.isDisplayed();
    }

    public SelenideElement  getwelcomeText() {
        return welcomeText;
    }

    public SelenideElement getIntructionIcon() {

        return intructionIcon;
    }

    public SelenideElement getChallengeIcon() {

        return challengeIcon;
    }

    public List<SelenideElement> getSolveButton() {

        return solveButton;
    }


    public SelenideElement getMandatoryAttribute() {

        return mandatoryAttribute;
    }

    public SelenideElement getFinishButton() {
        return finishButton;
    }


    public boolean isProblemsPageDisplayed() {
        return challengePage.isDisplayed();

    }

    public void clickOnInstructinIcon() {
        intructionIcon.click();
    }

    public void clickOnChallengeIcon() {
        challengeIcon.click();
    }

    public void selectTheProblem(int number) {
        solveButton.get(number - 1).shouldBe(Condition.visible).click();
    }

    public boolean isMandatoryAttributeDisplayed() {
        return mandatoryAttribute.isDisplayed();
    }

    public void clickFinishButton() {
        finishButton.click();

    }

}
