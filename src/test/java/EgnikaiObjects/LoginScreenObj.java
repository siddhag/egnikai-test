package EgnikaiObjects;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class LoginScreenObj {


    private static SelenideElement username = $(byId("username"));
    private static SelenideElement password = $(byId("password"));
    private static SelenideElement login = $(byText("Login"));
    private static SelenideElement startTestButton = $(byText("Start test"));
    private static SelenideElement startYourButton = $(byXpath("//button[contains(@class,'login-button')]"));
    private static SelenideElement finish = $(withText("Finish"));
    private static SelenideElement readInstruction = $(".button.is-primary");
    private static SelenideElement nextBtn= $(byXpath("//span[contains(text(),'Next')]/.."));
    private static SelenideElement nextSampleProblem = $(byXpath("//span[contains(text(),'See sample problem')]"));
    private static SelenideElement yes = $(byText("Yes"));
    private static SelenideElement welcomeToEgnikai=$(byXpath("//h1[text()='Welcome to Egnikai.']"));
    private static SelenideElement instructionText =$(byXpath("//h1[text()='Instructions']"));
    private static SelenideElement invalidloginErrormessage =$(".has-text-danger");
    private static SelenideElement invalidLoginMsg = $(byXpath("//div[contains(text(),'*Invalid username or password')]"));
    private static SelenideElement lifeatTW = $(byXpath("//a[@class='button is-primary is-large-button']"));

    public SelenideElement getUserNameEdt() { return username; }

    public SelenideElement getPasswordEdt() { return password; }

    public SelenideElement getLoginBtn() { return login; }

    public SelenideElement getStartTestButton() { return startTestButton; }

    public SelenideElement getStartYourButton() { return startYourButton; }

    public SelenideElement getFinishBtn() { return finish; }

    public SelenideElement getYesBtn() throws InterruptedException {
        return yes;
    }

    public SelenideElement getinvalidLoginMsg() throws InterruptedException {
        return invalidLoginMsg;
    }

    public SelenideElement getReadInstruction() {
        return readInstruction;
    }

    public SelenideElement getNextBtn() { return nextBtn; }

    public SelenideElement getNextSampleProblem() {
        return nextSampleProblem;
    }

    public boolean welcomeToEgnikaiIsDisplayed()
    {
        return welcomeToEgnikai.isDisplayed();
    }
    public boolean InstructionPageisDisplayed()
    {
        return instructionText.isDisplayed();
    }

    public String getInvalidLoginMessage() {return invalidloginErrormessage.getText();}

    public SelenideElement getlifeatTW() { return lifeatTW; }
}
