package EgnikaiObjects;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byTitle;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$x;

public class ProblemsObj {



    private final SelenideElement problemTextbox = $(byXpath("//div[@class='ace_content']"));

    private final SelenideElement saveBtn = $(byXpath("//button[contains(@class,'button is-primary m-r-20')]"));

    private final SelenideElement runBtn = $(byXpath("//button[contains(text(),'Run')]"));

    private final SelenideElement seeAllProblems = $(byXpath("//a[contains(text(),'See all problems')]"));

    private final SelenideElement title = $("h1[class^='title']");

    private  final SelenideElement editor =$(byXpath("//div[@class='ace_content']/div"));

    private final SelenideElement resultText=$(byXpath("//*[contains(text(),'Result')]"));

    private final SelenideElement testResult=$(byXpath("//*[contains(text(),'Result')]/div"));

    public SelenideElement getSrcCodeFile() {
        return srcCodeFile;
    }

    private final SelenideElement srcCodeFile=$x("//li[2]");


    public SelenideElement getProblemTextbox() {
        return problemTextbox;
    }

    public SelenideElement getEditor() {
        return editor;
    }

    public SelenideElement getTitle() {
        return title;
    }

    public SelenideElement getSaveBtn() {
        return saveBtn;
    }

    public SelenideElement getRunBtn() {
        return runBtn;
    }

    public SelenideElement getSeeAllProblems() {
        return seeAllProblems;
    }

    public SelenideElement getResult() { return resultText; }

    public SelenideElement getTestResult() { return testResult; }


}
