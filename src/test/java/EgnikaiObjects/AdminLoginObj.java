package EgnikaiObjects;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class AdminLoginObj {

    private static SelenideElement userNameAdmin = $(byId("username"));
    private static SelenideElement pwdNameAdmin = $(byId("password"));
    private static SelenideElement loginAdmin = $(byXpath("//button[contains(text(),'Login')]"));
    private static SelenideElement createEvent = $(byXpath("//a[contains(text(),'Create Event')]"));
    private static SelenideElement eventNameEdit = $(byXpath(" //input[@type='text']"));
    private static SelenideElement applyAllBtn = $(byXpath("//button[@class='btn btn-warning pull-right']"));
    private static SelenideElement submitBtn = $(byXpath(" //button[text()='Submit']"));
    private static SelenideElement activeSessionLink = $(byXpath("//a[text()='Active Sessions']"));
    private static SelenideElement pastSessionLink = $(byXpath("//a[contains(text(),'Past Sessions')]"));
    private static SelenideElement lockCandidateButton = $(withText("Lock Candidate/s"));
    private static SelenideElement unlockCandidateButton = $(withText("Unlock Candidate/s"));


    public SelenideElement getUserNameAdminEdt() { return userNameAdmin; }

    public SelenideElement getPasswordAdminEdt() {
        return pwdNameAdmin;
    }
    
    public SelenideElement getLoginAdminBtn() {
        return loginAdmin;
    }

    public SelenideElement getCreateEventBtn() { return createEvent; }

    public SelenideElement getEventNameEdit() {
        return eventNameEdit;
    }

    public static SelenideElement getApplyAllbtn() {
        return applyAllBtn;
    }

    public static SelenideElement getSubmitBtn() {
        return submitBtn;
    }

    public static SelenideElement getActiveSessionLink() {
        return activeSessionLink;
    }

    public static SelenideElement getPastSessionLink() {
        return pastSessionLink;
    }

    public static SelenideElement getCreateEvent() {
        return createEvent;
    }

    public static SelenideElement getScore(String userName) {
        return $x("//td[text()='" + userName + "']/../td[4]");
    }

    public static SelenideElement getGitRepo(String userName) {
        return $x("//td[text()='" + userName + "']/../td[5]/a");
    }


    public static SelenideElement getCandidateName(String userName) {
        return $x("//td[text()='" + userName + "']");
    }

    public static SelenideElement getCid(String userName) {
        return $x("//td[text()='" + userName + "']/../td[2]");
    }


    public SelenideElement getLockCandidateButton() {
        return lockCandidateButton;
    }

    public SelenideElement getUnlockCandidateButton() {
        return unlockCandidateButton;
    }

    public SelenideElement getLockCandidateIcon(String userName) {
        return $x("//td[text()='" + userName + "']/..//i");
    }

    public SelenideElement getDeactivateButton(String userName) {
        return $x("//td[text()='" + userName + "']/../td[9]/button");
    }


}
