#!/usr/bin/env bash
set -e
# $1 = environment(dev/QA),
# $2 = testSuiteConfigurationFile (AdminFunctionalTest.xml/CandidateTestng.xml`)
mvn clean install -Denv=$1 -DsuiteXmlFile=$2 -Dbrowser=chrome